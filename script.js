const fireBtn = document.getElementById("getLabel");
const fireBtnFake = document.getElementById("getBtn");
const clawPointer = document.getElementById("pointer");
const checkbox = document.getElementById("fire");
const prize1 = document.getElementById("prize1");
const prize2 = document.getElementById("prize2");
const prize3 = document.getElementById("prize3");
const prize1Door = document.getElementById("prize1-door");
const prize2Door = document.getElementById("prize2-door");
const prize3Door = document.getElementById("prize3-door");
const word1 = document.getElementById("word1");
const word2 = document.getElementById("word2");
const word3 = document.getElementById("word3");

const overlay = document.getElementById("overlay");
const popup = document.getElementById("popup");
const closeBtn = document.getElementById("closeBtn");
const coinsDisplay = document.getElementById("coins");
const secretWordDisplay = document.getElementById("secretWord");
var itsFailure = false;
var wordCounter = 0;

var secretWords = ["muchos", "detrás", "de mí"];
var wordsObtained = ["", "", ""];

overlay.style.visibility = "visible";
overlay.style.opacity = "1";

function restart() {
  itsFailure = false;
  wordCounter = 0;
  secretWords = ["muchos", "detrás", "de mí"];
  wordsObtained = ["", "", ""];
  secretWords.sort(comparacionAleatoria);
  prize1.style.opacity = "1";
  prize2.style.opacity = "1";
  prize3.style.opacity = "1";
  coinsDisplay.innerHTML = `
    <div>🪙</div>
    <div>🪙</div>
    <div>🪙</div>
    <div>🪙</div>`;
  secretWordDisplay.innerHTML = `
    <div>?</div>
    <div>?</div>
    <div>?</div>`;
  fireBtn.style.display = "block";
  fireBtnFake.style.display = "none";
  prize1.classList.remove("prize1_animation");
  prize1.classList.remove("prize1_miss_animation");
  prize2.classList.remove("prize2_animation");
  prize2.classList.remove("prize2_miss_animation");
  prize3.classList.remove("prize3_animation");
  prize3.classList.remove("prize3_miss_animation");
  prize1Door.classList.remove("get-prize-door");
  prize2Door.classList.remove("get-prize-door");
  prize3Door.classList.remove("get-prize-door");
  word1.classList.remove("word1_animation");
  word2.classList.remove("word2_animation");
  word3.classList.remove("word3_animation");
  secretWordDisplay.children[0].classList.remove("rotate-in-center");
  secretWordDisplay.children[1].classList.remove("rotate-in-center");
  secretWordDisplay.children[2].classList.remove("rotate-in-center");
  closePopup();
}

function comparacionAleatoria() {
  return Math.random() - 0.5;
}

// secretWords.sort(comparacionAleatoria);

closeBtn.addEventListener("click", function () {
  closePopup();
});

overlay.addEventListener("click", function (event) {
  if (event.target === overlay && itsFailure === false) {
    closePopup();
  }
});

function closePopup() {
  overlay.style.visibility = "hidden";
  overlay.style.opacity = "0";
}
const handleClick = () => {
  const lastChild = coinsDisplay.lastElementChild;

  if (lastChild && coinsDisplay.childElementCount > 1) {
    coinsDisplay.removeChild(lastChild);
  }
  if (coinsDisplay.childElementCount == 1) {
    lastChild.style.opacity = "0";
  }
};
const handleMouseUp = (isTouchend) => {
  if (isTouchend) {
    checkbox.checked = true;
  }

  fireBtn.style.display = "none";
  fireBtnFake.style.display = "block";
  let segundos = 6;

  const intervalo = setInterval(function () {
    segundos--;

    if (segundos <= 0) {
      clearInterval(intervalo);
      fireBtnFake.textContent = "6";
    } else {
      fireBtnFake.textContent = `${segundos}`;
    }
  }, 1000); 

  const rectPointer = clawPointer.getBoundingClientRect();
  const rectPrize1 = prize1.getBoundingClientRect();
  const rectPrize2 = prize2.getBoundingClientRect();
  const rectPrize3 = prize3.getBoundingClientRect();
  const centerPointer = parseInt(
    Math.ceil(rectPointer.left + rectPointer.width / 2 + window.scrollX)
  );
  const centerPrize1 = parseInt(
    Math.ceil(rectPrize1.left + rectPrize1.width / 2 + window.scrollX)
  );
  const centerPrize2 = parseInt(
    Math.ceil(rectPrize2.left + rectPrize2.width / 2 + window.scrollX)
  );
  const centerPrize3 = parseInt(
    Math.ceil(rectPrize3.left + rectPrize3.width / 2 + window.scrollX)
  );

  if (
    (centerPointer >= centerPrize1 - 30 && centerPointer <= centerPrize1 - 6) ||
    (centerPointer >= centerPrize1 + 6 && centerPointer <= centerPrize1 + 30)
  ) {
    prize1.classList.add("prize1_miss_animation");
  }
  if (
    centerPointer >= centerPrize1 - 5 &&
    centerPointer <= centerPrize1 + 5 &&
    window.getComputedStyle(prize1).getPropertyValue("opacity") == 1
  ) {
    wordCounter++;
    prize1.classList.add("prize1_animation");
    setTimeout(function () {
      prize1Door.classList.add("get-prize-door");
    }, 5500);
    revealWord(secretWords[0]);
  }

  if (
    (centerPointer >= centerPrize2 - 30 && centerPointer <= centerPrize2 - 6) ||
    (centerPointer >= centerPrize2 + 6 && centerPointer <= centerPrize2 + 30)
  ) {
    prize2.classList.add("prize2_miss_animation");
  }
  if (
    centerPointer >= centerPrize2 - 5 &&
    centerPointer <= centerPrize2 + 5 &&
    window.getComputedStyle(prize2).getPropertyValue("opacity") == 1
  ) {
    wordCounter++;
    prize2.classList.add("prize2_animation");
    setTimeout(function () {
      prize2Door.classList.add("get-prize-door");
    }, 5500);
    revealWord(secretWords[1]);
  }

  if (
    (centerPointer >= centerPrize3 - 30 && centerPointer <= centerPrize3 - 6) ||
    (centerPointer >= centerPrize3 + 6 && centerPointer <= centerPrize3 + 30)
  ) {
    prize3.classList.add("prize3_miss_animation");
  }
  if (
    centerPointer >= centerPrize3 - 5 &&
    centerPointer <= centerPrize3 + 5 &&
    window.getComputedStyle(prize3).getPropertyValue("opacity") == 1
  ) {
    wordCounter++;
    prize3.classList.add("prize3_animation");
    setTimeout(function () {
      prize3Door.classList.add("get-prize-door");
    }, 5500);
    revealWord(secretWords[2]);
  }

  setTimeout(function () {
    checkbox.checked = false;
    if (prize1.classList.contains("prize1_animation")) {
      prize1.style.opacity = "0";
    }
    if (prize2.classList.contains("prize2_animation")) {
      prize2.style.opacity = "0";
    }
    if (prize3.classList.contains("prize3_animation")) {
      prize3.style.opacity = "0";
    }
    prize1.classList.remove("prize1_animation");
    prize1.classList.remove("prize1_miss_animation");
    prize2.classList.remove("prize2_animation");
    prize2.classList.remove("prize2_miss_animation");
    prize3.classList.remove("prize3_animation");
    prize3.classList.remove("prize3_miss_animation");

    fireBtn.style.display = "block";
    fireBtnFake.style.display = "none";

    if (
      coinsDisplay.childElementCount == 1 &&
      window
        .getComputedStyle(coinsDisplay.lastElementChild)
        .getPropertyValue("opacity") == 0 &&
      wordCounter < 3
    ) {
      failure();
    }
  }, 6000);
};

const revealWord = (word) => {
  setTimeout(function () {
    if (word == "muchos") {
      word1.classList.add("word1_animation");
    }
    if (word == "detrás") {
      word2.classList.add("word2_animation");
    }
    if (word == "de mí") {
      word3.classList.add("word3_animation");
    }
  }, 6500);

  setTimeout(function () {
    if (word == "muchos") {
      secretWordDisplay.children[0].classList.add("rotate-in-center");
      secretWordDisplay.children[0].innerHTML = "<div>muchos</div>";
      wordsObtained[0] = "muchos";
    }
    if (word == "detrás") {
      secretWordDisplay.children[1].classList.add("rotate-in-center");
      secretWordDisplay.children[1].innerHTML = "<div>detrás</div>";
      wordsObtained[1] = "detrás";
    }
    if (word == "de mí") {
      secretWordDisplay.children[2].classList.add("rotate-in-center");
      secretWordDisplay.children[2].innerHTML = "<div>de mí</div>";
      wordsObtained[2] = "de mí";
    }
    if (
      wordsObtained[0] != "" &&
      wordsObtained[1] != "" &&
      wordsObtained[2] != ""
    ) {
      victory();
    }
  }, 9200);
};

const victory = () => {
  const jsConfetti = new JSConfetti();
  jsConfetti.addConfetti();
  fireBtn.style.display = "none";
  fireBtnFake.style.display = "block";
  popup.innerHTML = `
    <h2>muchos detrás de mí</h2>
    <h4>😎¡Lo lograste!🥳</h4>
  `;
  overlay.style.visibility = "visible";
  overlay.style.opacity = "1";
};
const failure = () => {
  itsFailure = true;
  fireBtn.style.display = "none";
  fireBtnFake.style.display = "block";

  popup.innerHTML = `
    <h2>😿 Lástima 🙁</h2>
    <h4>¡No te rindas!</h4>
    <button class="icono-btn" onclick="restart()">
      <span class="material-symbols-outlined"> restart_alt </span>
    </button>
  `;
  overlay.style.visibility = "visible";
  overlay.style.opacity = "1";
};

fireBtn.addEventListener("mousedown", handleClick);
fireBtn.addEventListener("touchstart", handleClick);
fireBtn.addEventListener("mouseup", handleMouseUp.bind(null, false));
fireBtn.addEventListener("touchend", handleMouseUp.bind(null, true));
